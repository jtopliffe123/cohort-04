

def fizzbuzz(number):
    if not 1 <= number <= 100:
        raise ValueError('Invalid number!')
    if number % 15 == 0:
        return 'fizzbuzz'
    if number % 3 == 0:
        return 'fizz'
    if number % 5 == 0:
        return 'buzz'
    return number

if __name__ == '__main__':
    for i in range(1, 101):
        print(fizzbuzz(i))
